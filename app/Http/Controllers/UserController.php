<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return "LISTA DE USUARIOS";
        //NOTA: En laravel las funciones incorporadas
        //se denominan "helpers", view es una de ella.
        $users = array('Javi', 'Ana', 'Diego', 'Sandra');

        // $users = array();

        return view('user.index', ['users' => $users]);
        //busca el fichero (uno de los dos):
        // /resources/views/user/index.php
        // /resources/views/user/index.blade.php
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all()); //var_dump mas sofisticado que recoje todos los parametros enviados
        //opcion1
        $user = new User;
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = bcrypt($request->input('password'));
        $user->request_token = str_random(10);//cadena aleatoria
        $user->save();
        //opcion2
        $user = new User::create($request->all());
        $user->save();
        //opcion3
        $user = new User();
        $user->fill($request->all());
        $user->save();
        return redirect('/users');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //opcion1
    public function show($id)
    {
        // $otro = "<script>alert('hola mundo')</script>";
        return view('user.show', [
            'id' => $id,
         ]);
    }
    //opcion2
    public function show(User $user)
    {
        // $otro = "<script>alert('hola mundo')</script>";
        return view('user.show', [
            'user' => $user,
         ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        return view('user.edit', ['user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $user->fill($request->all());
        $user->save();
        return redirect('/users/' . $user->id);
        //por definicion te lleva a la vista de ese usuario
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        //opcion2
        User::destroy($id);//podemos poner varios ids [$id1, $id2,...$idN];
        return back;
        //return redirect('users');
    }

    public function especial()
    {
        $users = User::where('id', '>=', 5)->where('id', '<=', 10)->get();
        dd($users);
        return redirect('/users');
        // return "Especial";
    }
}
