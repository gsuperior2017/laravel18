@extends('layouts.app')

@section('title', 'Usuarios')

@section('content')
    <h1>Editar usuario</h1>
    <form method="post" action="/users/{{$user->id}}">
        {{csrf_field()}}

        <input type="text" name="_method" value="put">
        <label>Nombre</label>
        <input type="text" name="name" value="{{$user->name}}">
        <br>
        <label>email   </label>
        <input type="text" name="email" value="{{$user->email}}">
        <input type="subtmi" name="submit" value="Modificar">

    </form>

@endsection
