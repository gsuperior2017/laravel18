<!DOCTYPE html>
<html>
<head>
    <title>Lista de usuarios</title>
</head>
<body>
    <h1>Es la lista de usuarios</h1>
    <ul>
    @foreach ($users as $user)
        <li>{{ $user }}</li>
    @endforeach
    </ul>

    <hr>
    <h1>Es la lista de usuarios</h1>
    <ul>
    @if (empty($users))
        <li>No hay usuarios!!</li>
    @else
        @foreach ($users as $user)
            <li>{{ $user->name }}{{$user->email}}
                <a href="/users/{{$user->id}}/edit/">Editar</a>
                <form method="POST" action="/users/{{$user->id}}">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="DELETE">
                    <input type="submit" value="eliminar">
                </form>
            </li>
        @endforeach
    @endif
    </ul>

    <hr>
    <h1>Es la lista de usuarios</h1>
    <ul>
    @forelse ($users as $user)
        <li>{{ $user }}</li>
    @empty
        <li>No hay usuarios!!</li>
    @endforelse
    </ul>

</body>
</html>
